package data;

import exceptions.NoDataException;
import exceptions.OutOfRangeMarkException;

import java.util.ArrayList;

public class Student {
    private ArrayList<AcademicDiscipline> disciplines = new ArrayList<>();
    public Student(ArrayList<AcademicDiscipline> disciplines){
        this.disciplines = disciplines;
    }
    int getMarkForDiscipline(String disciplineName) throws NoDataException, OutOfRangeMarkException {
        if(disciplines.isEmpty()){
            throw new NoDataException("No discipline for this Student");
        }
            for(int i = 0; i<disciplines.size(); i++){
                if(disciplines.get(i).getName() == disciplineName){
                    return disciplines.get(i).getMark();
                }
            }
            throw new NoDataException("No discipline called: " + disciplineName);
    }
    void addDiscipline(AcademicDiscipline discipline){
        disciplines.add(discipline);
    }
    ArrayList<AcademicDiscipline> getDisciplines(){
        return disciplines;
    }

    boolean containsDiscipline(String disciplineName){
        for(int i = 0; i<disciplines.size(); i++){
            if(disciplines.get(i).getName() == disciplineName){
                return true;
            }
        }
        return false;
    }

    double getAverageMark() throws NoDataException, OutOfRangeMarkException {
        int counter = 0;
        if(disciplines.isEmpty())
        {
            throw new NoDataException("No discipline for this student");
        }
        for(int i = 0; i< disciplines.size(); i++){
            counter += disciplines.get(i).getMark();
        }
        if(counter == 0){
            throw new NoDataException("No such discipline in the group");
        }
        return counter/disciplines.size();
    }
}
