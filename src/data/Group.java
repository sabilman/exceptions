package data;

import exceptions.NoDataException;
import exceptions.OutOfRangeMarkException;

import java.util.ArrayList;

public class Group {
    private ArrayList<Student> students = new ArrayList<>();
    public Group(ArrayList<Student> students){
        this.students = students;
    }
    public ArrayList<Student> getStudents(){
        return this.students;
    }
    public void addStudent(Student student){
        students.add(student);
    }

    double getAverageMarkForGroupInConcreteDiscipline(String lesson) throws NoDataException, OutOfRangeMarkException {
        double avgMark = 0;
        if(students.isEmpty()){
            throw new NoDataException("No students in the group");
        }
        for(int i = 0; i<students.size(); i++){
            if(students.get(i).containsDiscipline(lesson)){
                avgMark+= students.get(i).getMarkForDiscipline(lesson);
            }
        }
        if(avgMark == 0){
            throw new NoDataException("No such discipline in the group");
        }
        return avgMark;
    }
}
