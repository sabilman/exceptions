package data;

import exceptions.NoDataException;
import exceptions.OutOfRangeMarkException;

import java.util.ArrayList;

public class Faculty {
    private ArrayList<Group> groups = new ArrayList<>();
    public Faculty(ArrayList<Group> groups){
        this.groups = groups;
    }
    public void addGroup(Group group){
        groups.add(group);
    }
    public ArrayList<Group> getGroups() {
        return this.groups;
    }

    double getAverageMarkForDisciplineInFacultyGroup(int groupIndex, String lessonName) throws NoDataException, OutOfRangeMarkException {
        return groups.get(groupIndex).getAverageMarkForGroupInConcreteDiscipline(lessonName);
    }
    double getAverageMarkForDisciplineInFacultyGroups( String lessonName) throws NoDataException, OutOfRangeMarkException {
        double avgMark = 0;
        for(int i = 0; i<groups.size(); i++){
          avgMark += groups.get(i).getAverageMarkForGroupInConcreteDiscipline(lessonName);
        }
        if(avgMark == 0){
            throw new NoDataException("No lesson called " + lessonName + "found");
        }
        return avgMark;
    }

}
