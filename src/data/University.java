package data;

import exceptions.NoDataException;
import exceptions.OutOfRangeMarkException;

import java.util.ArrayList;

public class University {
    ArrayList<Faculty> faculties = new ArrayList<>();

    public University(ArrayList<Faculty> faculties){
        this.faculties = faculties;
    }

    double getAverageMarkForCurrentDisciplineInUniversity(String lessonName) throws NoDataException, OutOfRangeMarkException {
        int avgMark = 0;
        if(faculties.isEmpty()){
            throw new NoDataException("No faculties in university");
        }
        for(int i = 0; i<faculties.size(); i++){
            avgMark += faculties.get(i).getAverageMarkForDisciplineInFacultyGroups(lessonName);
        }
        return avgMark;
    }
}
