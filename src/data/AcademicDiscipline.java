package data;

import exceptions.OutOfRangeMarkException;

public class AcademicDiscipline {
    private String name;
    private int mark;
    public AcademicDiscipline(String name, int mark){
        this.name = name;
        this.mark = mark;
    }
    public String getName(){
        return name;
    }
    public int getMark() throws OutOfRangeMarkException {
        if(mark<0||mark>10){
            throw new OutOfRangeMarkException();
        }
        return mark;
    }
}
