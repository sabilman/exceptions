package exceptions;

public class NoDataException extends Exception{
    public NoDataException(String e){
        super(e);
    }
}
