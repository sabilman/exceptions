package exceptions;

public class OutOfRangeMarkException extends Exception{
    public OutOfRangeMarkException(){
        super("Mark is less then 0 or higher then 10");
    }
}
